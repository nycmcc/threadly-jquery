const main = function() {

    $('form').submit(function(event) {
        let $input = $(event.target).find('input');
        let comment = $input.val();

        if (comment != "") {
            const html = $('<li>').text(comment);
            html.prependTo('#comments');
            $input.val("");
        }

        return false;
    });

}

$(document).ready(main);